import logging
import argparse
import time


from brainflow import LogLevels, BrainFlowModelParams, BrainFlowMetrics, BrainFlowClassifiers, MLModel
from brainflow.board_shim import BoardShim, BrainFlowInputParams, BoardIds
from brainflow.data_filter import DataFilter, FilterTypes, DetrendOperations
from PyQt5 import QtCore, QtGui

from circular_buffer import CircularBuffer
from pythonosc import udp_client

class Graph:
    def __init__(self, board_shim):
        self.first = True
        self.board_id = board_shim.get_board_id()
        self.board_shim = board_shim
        self.exg_channels = BoardShim.get_exg_channels(self.board_id)
        self.sampling_rate = BoardShim.get_sampling_rate(self.board_id)
        self.update_speed_ms = 125
        self.window_size = 4
        self.numNew = 32

        self.event_time = -1

        self.num_points = self.window_size * self.sampling_rate
        self.app = QtGui.QGuiApplication([])
        '''
        We add a circular buffer to be used for detection
        '''
        self.buff = CircularBuffer(self.sampling_rate * 8)


        '''
        Here we create a timer that will execute the function self.update every 125ms
        This allows for handling data in real time as it is received from the device.
        '''
        time.sleep(5)


        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(self.update_speed_ms)
        QtGui.QGuiApplication.instance().exec_()

    '''
    For the update function we execute it every 125ms.
    The device has a sampling rate of 256Hz.
    That means that in the 50ms we get 32 new samples
    THIS VALUE FOUND IN self.numNew
    '''
    def update(self):
        data = self.board_shim.get_current_board_data(self.num_points)
        bands = DataFilter.get_avg_band_powers(data, self.exg_channels, self.sampling_rate, True)
        feature_vector = bands[0]

        mindfulness_params = BrainFlowModelParams(BrainFlowMetrics.MINDFULNESS.value,
                                                  BrainFlowClassifiers.DEFAULT_CLASSIFIER.value)
        mindfulness = MLModel(mindfulness_params)
        mindfulness.prepare()
        mindfulness_data = str(mindfulness.predict(feature_vector))
        mindfulness.release()

        blink = False
        if time.time() - self.event_time >= 1:
            data2 = data[4] ### [4] added in order to only select the channel used for blinking
            DataFilter.detrend(data2, DetrendOperations.CONSTANT.value)
            DataFilter.perform_bandpass(data2, self.sampling_rate, 3.0, 45.0, 2,
                                        FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)
            DataFilter.perform_bandstop(data2, self.sampling_rate, 48.0, 52.0, 2,
                                        FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)
            DataFilter.perform_bandstop(data2, self.sampling_rate, 58.0, 62.0, 2,
                                        FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)

            for v in data2:
                self.buff.enqueue(v)

            avg = self.buff.average()
            std = self.buff.standard_deviation()
            threshold = avg - 4*std
            for v in data2[-self.numNew:]:
                if v < threshold:
                    blink = True
                    self.event_time = time.time()

        #Sending data over OSC to UE5
        client = udp_client.SimpleUDPClient("127.0.0.2", 8001)

        packed_boolean = 0
        if blink:
            packed_boolean = 1

        focus_level = float(mindfulness_data.strip("[]"))  # Pack string (convert to bytes)
        msg = "/test/me"  # OSC address
        args = [focus_level, packed_boolean]  # Arguments

        # Send the OSC message
        client.send_message(msg, args)
        self.app.processEvents()


def main():
    BoardShim.enable_dev_board_logger()
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    # use docs to check which parameters are required for specific board, e.g. for Cyton - set serial port
    parser.add_argument('--timeout', type=int, help='timeout for device discovery or connection', required=False,
                        default=0)
    parser.add_argument('--ip-port', type=int, help='ip port', required=False, default=0)
    parser.add_argument('--ip-protocol', type=int, help='ip protocol, check IpProtocolType enum', required=False,
                        default=0)
    parser.add_argument('--ip-address', type=str, help='ip address', required=False, default='')
    parser.add_argument('--serial-port', type=str, help='serial port', required=False, default='')
    parser.add_argument('--mac-address', type=str, help='mac address', required=False, default='')
    parser.add_argument('--other-info', type=str, help='other info', required=False, default='')
    parser.add_argument('--streamer-params', type=str, help='streamer params', required=False, default='')
    parser.add_argument('--serial-number', type=str, help='serial number', required=False, default='')
    parser.add_argument('--board-id', type=int, help='board id, check docs to get a list of supported boards',
                        required=False, default=BoardIds.MUSE_S_BOARD)
    parser.add_argument('--file', type=str, help='file', required=False, default='')
    parser.add_argument('--master-board', type=int, help='master board id for streaming and playback boards',
                        required=False, default=BoardIds.NO_BOARD)
    args = parser.parse_args()

    params = BrainFlowInputParams()
    params.ip_port = args.ip_port
    params.serial_port = args.serial_port
    params.mac_address = args.mac_address
    params.other_info = args.other_info
    params.serial_number = args.serial_number
    params.ip_address = args.ip_address
    params.ip_protocol = args.ip_protocol
    params.timeout = args.timeout
    params.file = args.file

    board_shim = BoardShim(args.board_id, params)
    try:
        board_shim.prepare_session()
        board_shim.start_stream(450000, args.streamer_params)
        Graph(board_shim)
    except BaseException:
        logging.warning('Exception', exc_info=True)
    finally:
        logging.info('End')
        if board_shim.is_prepared():
            logging.info('Releasing session')
            board_shim.release_session()


if __name__ == "__main__":
    main()

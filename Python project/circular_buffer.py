import numpy as np
from brainflow.data_filter import DataFilter, DetrendOperations, FilterTypes

class CircularBuffer:
    def __init__(self, capacity):
        self.capacity = capacity
        self.buffer = [None] * capacity
        self.size = 0
        self.head = 0
        self.tail = 0

    def is_empty(self):
        return self.size == 0

    def is_full(self):
        return self.size == self.capacity

    def enqueue(self, item):
        if self.is_full():
            # If buffer is full, overwrite the oldest item
            self.dequeue()
        self.buffer[self.tail] = item
        self.tail = (self.tail + 1) % self.capacity
        self.size += 1

    def dequeue(self):
        if self.is_empty():
            return None
        item = self.buffer[self.head]
        self.buffer[self.head] = None
        self.head = (self.head + 1) % self.capacity
        self.size -= 1
        return item

    def peek(self):
        if self.is_empty():
            return None
        return self.buffer[self.head]

    def __len__(self):
        return self.size

    def __str__(self):
        return str(self.buffer)

    def average(self):
        if self.is_empty():
            return None
        values = [x for x in self.buffer if x is not None]
        return np.mean(values)

    def standard_deviation(self):
        if self.is_empty():
            return None
        values = [x for x in self.buffer if x is not None]
        return np.std(values)

    def apply_filters(self, sampling_rate):
        if self.is_empty():
            return

        # Convert circular buffer to a list
        data_list = [x for x in self.buffer if x is not None]

        # Apply detrend operation
        DataFilter.detrend(data_list, DetrendOperations.CONSTANT.value)

        # Apply bandpass filters
        DataFilter.perform_bandpass(data_list, sampling_rate, 3.0, 45.0, 2,
                                    FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)

        # Apply bandstop filters
        DataFilter.perform_bandstop(data_list, sampling_rate, 48.0, 52.0, 2,
                                    FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)
        DataFilter.perform_bandstop(data_list, sampling_rate, 58.0, 62.0, 2,
                                    FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)

        # Update circular buffer with filtered data
        self.buffer = data_list + [None] * (self.capacity - len(data_list))
        self.head = 0
        self.tail = len(data_list) % self.capacity
        self.size = len(data_list)

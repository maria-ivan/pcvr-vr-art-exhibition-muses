# PC VR Setup with Muse S BCI

This repository contains a project for a PC VR setup used in an artistic experience with the Muse S BCI. The repository includes two main folders: an Unreal Engine project and a Python project.


## About the Project

This project integrates input from the Muse S BCI into a VR artistic experience. The VR experience is developed using Unreal Engine 5.3.2 and leverages the LumaAI plugin. The Python project is used to process data from the Muse S BCI and stream it via OSC.

## Features

- Real-time input from Muse S BCI.
- Artistic visualizations in a VR environment.
- Scene transitions triggered by blinking.
- Explosion effects controlled by focus levels.

## Getting Started

### Prerequisites

Ensure you have the following installed on your system:

1. **Unreal Engine 5.3.2**: Download from the [Epic Games Launcher](https://www.unrealengine.com/).
2. **LumaAI Plugin**: Follow the plugin installation instructions provided by LumaAI.
3. **Python 3.8+**: Install from the [official Python website](https://www.python.org/).
4. **Muse S BCI**: Required for extracting biometric data.
5. **Virtual Desktop**: For streaming the VR experience to the Meta Quest 2 headset.

### Installation

1. **Clone the Repository**:
   ```bash
   git clone git@gitlab.com:maria-ivan/pcvr-vr-art-exhibition-muses.git
   cd pcvr-vr-art-exhibition-muses
   ```

2. **Unreal Engine Project**:
   - Open the Unreal Engine project located in the `UE_Project` folder.
   - Ensure the LumaAI plugin is installed and enabled.
   - Build the project as an executable (EXE).

3. **Python Project**:
   - Navigate to the `Python_Project` folder.
   - Install the required libraries using pip:
     ```bash
     pip install PyQt5==5.15.7 PyQt5-Qt5==5.15.2 PyQt5-sip==12.11.0 pyqtgraph==0.12.4 brainflow
     ```
   - Additionally, install any other libraries mentioned in the imports.

### Usage

1. **Setup Virtual Desktop**:
   - Install Virtual Desktop on your Meta Quest 2 headset.
   - Ensure Virtual Desktop is configured correctly for streaming from your PC.

2. **Run the Python Script**:
   - Put on the Muse S BCI.
   - Start the Python script located in the `Python_Project` folder.
   - The script will connect to the Muse S and start an OSC stream on `127.0.0.2:8001` with the extracted features.

3. **Launch the Unreal Engine Project**:
   - Run the built EXE file from the Unreal Engine project.
   - Stream the VR experience to your headset using Virtual Desktop.
   - In the VR experience, blinking will transition between scenes, and focus levels will control explosion effects.
